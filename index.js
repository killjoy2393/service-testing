function testService(events) {
  let outPeople = events.filter((el) => {
    return el[1] == 'out'
  })
  let insidePeople = events.filter((el) => {
    return el[1] == 'in'
  })

  let personal = events.filter((el) => {
    return el[0]
  })

  outPeople = outPeople.length
  insidePeople = insidePeople.length

  if (outPeople == insidePeople) {
    return true
  } else if (insidePeople != outPeople) {
    return false
  }

  // Оставил всё, что работает. Ума не приложу, как написать одну функцию для все тестов
}

module.exports = testService
